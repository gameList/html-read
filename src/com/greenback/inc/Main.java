package com.greenback.inc;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		File input = new File("C:\\input\\");
		File dir = new File("C:\\input\\results\\");
		
		if(!dir.exists())
		{
			new File("C:\\input\\results\\").mkdirs();
		}
		
		File[] directoryListing = input.listFiles();
		
		if(directoryListing != null)
		{
			HTMLReader reader = null;
			for(File child : directoryListing)
			{
				//loop through all files ending in .html and pass to HTMLReader
				int i = child.toString().lastIndexOf('.');
				if(i > 0 && child.toString().substring(i + 1).equalsIgnoreCase("html"))
				{
					reader = new HTMLReader(child);
					reader.readHtml();
					reader.write();
				}
			}
		}
		
		//HTMLReader reader = new HTMLReader(input);
		
		//reader.readHtml();
	    //System.out.println(reader.toString());
	    
	    //reader.write();
	}

}