package com.greenback.inc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLReader {
	
	private File input;
	
	private Elements htmlRead;
	
	private String ref;
	private String date;
	private String currency;
	private String amount;
	
	private JSONObject jsonObject;

	public File getInput() {
		return input;
	}

	public Elements getHtmlRead() {
		return htmlRead;
	}
	
	//Below are functions to set variables above
	
	public HTMLReader(File input)
	{
		//so we know what the file path should be
		this.input = input;
		
		this.ref = new String();
		this.date = new String();
		this.currency = new String();
		this.amount = new String();
	}
	
	//if file is to be added in later
	public HTMLReader()
	{
		this.ref = new String();
		this.date = new String();
		this.currency = new String();
		this.amount = new String();
	}
	
	//this function strips the required values from the input file if it was provided when the object was created
	public void readHtml()
	{
		String tempRef = "";
		String tempDate = "";
		String tempTotal = "";
		try {
			Document doc = Jsoup.parse(input, "UTF-8");
			
			//Grab the required information for the output
			Element element = doc.getElementsMatchingOwnText("Invoice #:").first();
			tempRef = element.wholeText();
			
			element = doc.getElementsMatchingOwnText("Date:").first();
			tempDate = element.wholeText();
			
			element = doc.getElementsMatchingOwnText("Grand Total:").first();
			tempTotal = element.wholeText();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//fetch the correct data from each element
		if(!tempRef.equalsIgnoreCase(""))
		{
			this.ref = tempRef.replace("Invoice #: ", "");
		}
		
		if(!tempDate.equalsIgnoreCase(""))
		{
			//format date to yyyy-MM-dd after striping all unrelvented parts of the string
			Date tDate = null;
			tempDate = tempDate.replace("Date: ", "");
			
			if(Character.isLetter(tempDate.charAt(0)))
			{
				try {
					tDate = new SimpleDateFormat("MMM dd, yyyy").parse(tempDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				try {
					tDate = new SimpleDateFormat("MM/dd/yyyy").parse(tempDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			LocalDateTime myDate = LocalDateTime.ofInstant(tDate.toInstant(), ZoneId.systemDefault());
			DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			
			date = myDate.format(myFormat);
		}
		
		if(!tempTotal.equalsIgnoreCase(""))
		{
			tempTotal = tempTotal.replace("Grand Total: $", "");
			
			for(int i = 0; i < tempTotal.length(); i++)
			{
				
				if(Character.isLetter(tempTotal.charAt(i)))
				{
					currency += tempTotal.charAt(i);
				}
				else
				{
					amount += tempTotal.charAt(i);
				}
			}
			//trim the strings
			if(currency.equalsIgnoreCase(""))
			{
				currency = "USD";
			}
			currency.trim();
			amount.trim();
		}
	}
	
	public void write()
	{
		jsonObject = new JSONObject();
		
		jsonObject.put("date",date);
		jsonObject.put("ref", ref);
		jsonObject.put("currency", currency);
		jsonObject.put("amount", Double.valueOf(amount));
		
		try {
			int i = input.toString().lastIndexOf('.');
			String jsonOutputName = input.toString().substring(0,i);
			
			i = jsonOutputName.lastIndexOf('\\');
			jsonOutputName = jsonOutputName.substring(i+1);
			
			File file = new File("C:\\input\\results\\" + jsonOutputName + ".json");
			file.createNewFile();
			
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(jsonObject.toJSONString());
			
			fileWriter.flush();
			fileWriter.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString()
	{
		return "ref:      " + this.ref + "\n" +
			   "date:     " + this.date +"\n"+
			   "currency: " + this.currency + "\n"+
			   "amount:   " + this.amount + "\n";
	}

}
